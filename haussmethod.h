#ifndef HAUSSMETHOD_H
#define HAUSSMETHOD_H


#include <array>
#include <vector>
#include <iostream>
#include <cmath>
#include <functional>

using namespace std;

template <typename T, size_t D>
using Matrix = array<array<T, D>, D>;

class HaussMethod{
private:
    Matrix<double, 3> matrix;
    vector<double> yColumn;
    vector<double> polynom;
public:
    HaussMethod(Matrix<double, 3> matrix_n, vector<double> yColumn_n);
    vector<double> solveMatrix();
};
#endif // HAUSSMETHOD_H
