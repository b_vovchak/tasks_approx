#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "haussmethod.h"

#include <QDebug>
#include <QChart>
#include <QFile>
#include <QDir>
#include <QFileDialog>
#include <vector>
#include <array>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <cmath>
#include <QThread>
#include <sstream>

using namespace std;

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    timer = new QTimer(this);
    connect(timer, SIGNAL(timeout()), this, SLOT(updateCaption()));
    connect(ui->pButtSelect, &QPushButton::clicked, this, &MainWindow::selectFileName);
    connect(ui->BStart, &QPushButton::clicked, this, &MainWindow::BStart_clicked);
    connect(this, SIGNAL(calculateFinished()), this, SLOT(startDrawing()), Qt::ConnectionType::QueuedConnection);
    inFunction = new QLineSeries();
    outFunction = new QSplineSeries();
    chart = new QChart();
    progress = 0;
    ui->PBInformation->setText(" ");
}

MainWindow::~MainWindow(){
    delete ui;
}

void MainWindow::startDrawing(){
   ui->PBInformation->setText("Drawing...");
    int localgap = 250000 / points.size();
    for(auto& point : points){
        inFunction->append(point.x, point.y);
        outFunction->append(point.x, polynom[0] + (polynom[1]*point.x) + (polynom[2]*(point.x*point.x)));
        progress += localgap;
    }
    inFunction->setName("Input function");
    outFunction->setName("Approximated function");
    if(chart->series().count()){
        chart->removeSeries(inFunction);
        chart->removeSeries(outFunction);
    }
    chart->addSeries(inFunction);
    chart->addSeries(outFunction);
    chart->createDefaultAxes();
    chart->setTitle("Approximation of function");
    chart->createDefaultAxes();
    chart->setTitle("Approximation of function");
    ui->graphicsView1->setRenderHint(QPainter::Antialiasing);
    ui->graphicsView1->setChart(chart);
    progress = 750000;
    ui->PBInformation->setText("Done");
}

void MainWindow::onClickME_Clicked(){
}

void MainWindow::selectFileName(){
    m_fileName = QFileDialog::getOpenFileName(this, "Select file...", QDir::homePath());
    ui->lineEdit->setText(m_fileName);
}

void MainWindow::calculate(){
   double progressgap;
    size_t approxType = 3;
    double x, y;
    string line;
    ifstream inputFile(m_fileName.toStdString());

    points.clear();
    if (inputFile.is_open())
    {
        getline(inputFile, line);
        inputFile.seekg(0, ios::end);
        progressgap = 250000/(inputFile.tellg()/line.size());
        inputFile.seekg(0, ios::beg);
        while (getline(inputFile, line)) {
            if (line.length() > 0) {
                std::istringstream l_stream(line);
                l_stream >> x;
                l_stream >> y;
                points.push_back(Point(x, y));
            }
            progress += progressgap;
            ////////////////////////////////
            QThread::msleep(40);
        }
        progress = 250000;
        cout << "Read " << points.size() << " points" << endl;
        inputFile.close();
    }
    else {
        cout << "Cannot open file. Filename isn't correct or file doesn't exist.\nExiting...\n";
        progress = 0;
        ui->PBInformation->setText("Cannot open file. Filename isn't correct or file doesn't exist.");
        return;
    }
    auto localProgress = (points.size()*(approxType-1)*approxType)+approxType*points.size();
    Matrix<double, 3> matrix;
    vector<double> yColumn(3);
    cout << "a1\ta2\ta3\tb" << endl << endl;
    for (size_t i = 0; i < approxType; ++i) {
        for (size_t j = 0; j < approxType; ++j) {
            matrix[i][j] = 0;
            if (!i && !j) {
                matrix[i][j] = points.size();

            }
            else {
                for (size_t v = 0; v < points.size(); ++v) {
                    matrix[i][j] += pow(points[v].x, i + j);
                    progress += localProgress;
                    QThread::msleep(10);
                }
            }
        }
        yColumn[i] = 0;
        for (size_t y = 0; y < points.size(); ++y) {
            yColumn[i] += points[y].y * pow(points[y].x, i);
            progress += localProgress;
            QThread::msleep(10);
        }
    }
    //progress = 500000;
    for (size_t i = 0; i < 3; ++i) {
        for (size_t j = 0; j < 3; ++j) {
            cout << matrix[i][j] << "\t";
        }
        cout << yColumn[i] << endl;
    }
    polynom = HaussMethod(matrix, yColumn).solveMatrix();
    cout << endl;
    if(polynom.size() == 1){
        ui->PBInformation->setText("System doesn't have roots");
        progress = 0;
    } else {
        for (size_t i = 0; i < 3; ++i) {
            cout << polynom[i] << " x" << i << "\t";
        }
        emit MainWindow::calculateFinished();
    }
    return;
};


void MainWindow::updateCaption(){
    ui->PBMain->setValue(progress);
    if(progress == 750000){
        timer->stop();
    }
}

void MainWindow::BStart_clicked()
{
    ui->PBMain->setMaximum(750000);
    ui->PBInformation->setText("Calculating...");
    timer->setInterval(20);
    timer->start();
    inFunction->removePoints(0, inFunction->count());
    outFunction->removePoints(0, outFunction->count());
    ui->graphicsView1->update();
    readThread = new std::thread(&MainWindow::calculate, this);
}

