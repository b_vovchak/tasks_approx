#include <cmath>
#include <iostream>
#include <fstream>
#include <string>
#include "func_list.h"

using namespace std;


int main(int argc, char* argv[]) {
    if(argc<2){
        cout<<"There are not enougth parameters to run program. \nExiting...";
        return -3;
    }


    ifstream inputfile(argv[1]);
    ofstream outputfile(argv[2]);
    double* arrX;
    double* arrY;
    double* polynom;
    double accuracy;



    int arrSize;
    int approxType;


    if (inputfile.is_open()) {
        inputfile >> arrSize >> approxType >> accuracy;
        if (arrSize <= 0) {
            cout << "Wrong size\n";
            system("pause");
            return -2;
        }
        if (approxType <= 1) {
            cout << "Wrong type of approx.\n";
            return -2;
        }
        if (approxType > 3) {
            cout << "Wrong type of approx.\n";
            return -2;
        }
        arrX = new double[arrSize]();
        arrY = new double[arrSize]();
        polynom = new double[approxType]();
        for (int i = 0; i < arrSize; ++i) {
            inputfile >> arrX[i] >> arrY[i];
        }
    }
    else {
        cout << "Cannot open the file\n";
        system("pause");
        return -1;
    }
    double minX = arrX[0];
    double maxX = arrX[0];
    for (int i = 1; i < arrSize; ++i) {
        if (minX > arrX[i])
            minX = arrX[i];
        if (maxX < arrX[i])
            maxX = arrX[i];
    }


    double** functionsMatrix = new double* [approxType]();
    double* yColumn = new double[approxType]();
    for (int i = 0; i < approxType; ++i) {
        functionsMatrix[i] = new double[approxType + 1]();
    }
    //Creating Column with results for functions system
    for (int row = 0; row < approxType; ++row) {
        for (int element = 0; element < arrSize; ++element) {
            if (!row) {
                yColumn[row] += arrY[element];
            } else {
                yColumn[row] += arrY[element] * pow(arrX[element], row);
            }
        }
    }
    //Creating functions system
    for (int row = 0; row < approxType; ++row) {
        for (int element = 0; element < approxType; ++element) {
            if (!row && !element) {
                functionsMatrix[row][element] = arrSize;
                double temp = functionsMatrix[row][element];
                temp += 0;
            }
            else {
                for (int i = 0; i < arrSize; ++i) {
                    functionsMatrix[row][element] += pow(arrX[i], row + element);
                    double temp = functionsMatrix[row][element];
                    temp += 0;
                }
            }
        }
    }
    //Hauss Method



    if (HaussMethod(functionsMatrix, approxType, yColumn, polynom)==-1) {
        cout << "No roots. ARG = 0";
        system("pause");
        return -3;
    }
    //output to .csv
    outputfile << "X , F(x)" << endl;
    double fx = 0;
    for (double x = minX; x < maxX; x += accuracy) {
        for (int i = 0; i < approxType; ++i) {
            if (!i)
                fx = polynom[0];
            else {
                fx += polynom[i] * pow(x, i);
            }
        }
        outputfile << x << ", " << fx << endl;
    }
    std::string outstring;
    if (approxType == 2)
        outstring = "Linear approximation";
    if (approxType == 3)
        outstring = "Parabolic approximation";

    outstring.append(" with step " + to_string(accuracy) + " in bounds[" + to_string(minX) + " | " + to_string(maxX) + "]");

    outputfile << outstring << endl;
    system("pause");
    delete[] yColumn;
    outputfile.close();
    inputfile.close();
    for (int i = 0; i < approxType; ++i) {
        delete[] functionsMatrix[i];
    }
    delete[] functionsMatrix;
    delete[] arrX;
    delete[] arrY;
    return 0;

}
