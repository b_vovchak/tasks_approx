#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QChart>
#include <QtCharts>
#include <QLineSeries>
#include <thread>
#define PROGRESS_MAXIMUM_VALUE 1000000

using namespace std;

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE


struct Point {
    Point(double x_n, double y_n){
        x = x_n;
        y = y_n;
    }
    double x;
    double y;
};

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void calculate();

private:
    Ui::MainWindow *ui;
    std::thread* readThread;
    QChart* chart;
    QLineSeries* inFunction;
    QSplineSeries* outFunction;
    QTimer *timer;
    QString m_fileName;
    vector<Point> points;
    vector<double> polynom;
    int progress = 0;

signals:
    void calculateFinished();
public slots:
    void startDrawing();
private slots:
    void onClickME_Clicked();
    void selectFileName();
    void BStart_clicked();
    void updateCaption();
};
#endif // MAINWINDOW_H
