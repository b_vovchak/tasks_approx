#include "haussmethod.h"

HaussMethod::HaussMethod(Matrix<double, 3> matrix_n, vector<double> yColumn_n) {
    matrix = matrix_n;
    yColumn = yColumn_n;
}

vector<double> HaussMethod::solveMatrix(){
    //sorting
    double maxElem, dTempElem;
    size_t maxElemPos;
    bool sorted = false;
    while (!sorted) {
        sorted = true;
        for (size_t i = 0; i < matrix.size(); i++) {
            maxElem = fabs(matrix[i][i]);
            maxElemPos = i;
            for (size_t  j= i + 1; j < matrix.size(); j++) {
                if (fabs(matrix[j][i]) > maxElem) {
                    maxElemPos = j;
                    maxElem = fabs(matrix[j][i]);
                    sorted = false;
                }
            }
            if (maxElem == 0) {
                std::cout << "System doesnt have roots.\n";
               yColumn.resize(1);
                return yColumn;
            }
            //printf("------------------------------------------------\n");
            if (i != maxElemPos) {
                cout << "------------------------------------------------\n";
                for (size_t j = 0; j < matrix.size(); j++) {
                    dTempElem = matrix[i][j];
                    matrix[i][j] = matrix[maxElemPos][j];
                    matrix[maxElemPos][j] = dTempElem;
                }
                dTempElem = yColumn[i];
                yColumn[i] = yColumn[maxElemPos];
                yColumn[maxElemPos] = dTempElem;
                for (size_t q = 0; q < matrix.size(); ++q) {
                    for (size_t z = 0; z < matrix.size(); ++z)
                        cout << matrix[q][z] << "\t";
                    cout << yColumn[q] << endl;
                }
                cout << "------------------------------------------------\n";
            }
        }
    }
    for (size_t i = 0; i < matrix.size(); i++) {
        double generalElement = matrix[i][i];
        matrix[i][i] = 1;
        for (size_t j = i + 1; j < matrix.size(); j++) {
            matrix[i][j] = matrix[i][j] / generalElement;
        }
        yColumn[i] /= generalElement;
        for (size_t j = i + 1; j < matrix.size(); j++) {
            double temp = matrix[j][i];
            matrix[j][i] = 0;
            if (temp != 0) {
                for (size_t z = i + 1; z < matrix.size(); z++) {
                    matrix[j][z] -= temp * matrix[i][z];
                    //progressFunction();
                }
                yColumn[j] -= temp * yColumn[i];
            }
        }
        //progressFunction();
    }
    for (size_t q = 0; q < matrix.size(); ++q) {
        for (size_t z = 0; z < matrix.size(); ++z)
            cout << matrix[q][z] << "\t";
        cout << yColumn[q] << endl;
    }
    cout << "------------------------------------------------\n";
    polynom.resize(3);
    for (int i = matrix.size() - 1; i >= 0; i--) {
        polynom[i] = 0;
        double tempY = yColumn[i];
        for (size_t j = matrix.size() - 1; j > i; j--) {
            tempY = tempY - matrix[i][j] * polynom[j];
        }
        polynom[i] = tempY;
    }
    return polynom;
}
